package com.duxcast.smartnotes.dagger

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.provider.MediaStore
import android.support.v7.app.AlertDialog
import android.widget.Toast
import com.duxcast.smartnotes.R
import com.duxcast.smartnotes.activities.NewNoteActivity
import dagger.Module
import dagger.Provides

@Module
class ChooseImageDialogModule() {

    @Provides
    fun provideAlertDialog(context: Context):AlertDialog {
        var builder: AlertDialog.Builder = AlertDialog.Builder(context)
        builder.setTitle(R.string.chose).setItems(R.array.choose_image_dialog, { dialogInterface: DialogInterface, i: Int ->
            when {
                (i==0) -> {
                    if (context is Activity) {
                        val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        context.startActivityForResult(takePicture, 0)
                    }
                }
                (i==1) -> {
                    if (context is Activity) {
                        val pickPhoto = Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                        context.startActivityForResult(pickPhoto, 1)
                    }
                }
            }
        })
        return builder.create()
    }


}