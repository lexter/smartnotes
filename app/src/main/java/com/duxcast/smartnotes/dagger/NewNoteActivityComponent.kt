package com.duxcast.smartnotes.dagger

import com.duxcast.smartnotes.activities.NewNoteActivity
import dagger.Component

@Component(modules = arrayOf(ContextModule::class,ChooseImageDialogModule::class))
interface NewNoteActivityComponent {
    fun inject(newNoteActivity: NewNoteActivity)
}