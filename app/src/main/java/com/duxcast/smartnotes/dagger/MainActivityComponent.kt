package com.duxcast.smartnotes.dagger

import com.duxcast.smartnotes.activities.MainActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ContextModule::class))
interface MainActivityComponent {
    fun inject(mainActivity: MainActivity)
}