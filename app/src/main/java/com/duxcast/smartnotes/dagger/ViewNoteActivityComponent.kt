package com.duxcast.smartnotes.dagger

import com.duxcast.smartnotes.activities.ViewNoteActivity
import dagger.Component

@Component(modules = arrayOf(ContextModule::class,ChooseImageDialogModule::class))
interface ViewNoteActivityComponent {
    fun inject(viewNoteActivity: ViewNoteActivity)
}