package com.duxcast.smartnotes.dagger

import com.duxcast.smartnotes.activities.EditNoteActivity
import dagger.Component

@Component(modules = arrayOf(ContextModule::class,ChooseImageDialogModule::class))
interface EditNoteActivityComponent {
    fun inject(editNoteActivity: EditNoteActivity)
}