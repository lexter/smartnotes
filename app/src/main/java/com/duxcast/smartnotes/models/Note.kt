package com.duxcast.smartnotes.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

open class Note(@PrimaryKey var id:String = UUID.randomUUID().toString(),
                var title: String = "",
                var text: String = "",
                var priority: Int = 0,
                var photo:String? = null):RealmObject()