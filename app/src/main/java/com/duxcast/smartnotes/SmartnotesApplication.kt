package com.duxcast.smartnotes

import android.app.Application
import com.bumptech.glide.request.target.ViewTarget
import io.realm.Realm
import io.realm.RealmConfiguration

class SmartnotesApplication:Application() {

    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
        val config = RealmConfiguration.Builder()
                .build()
        Realm.setDefaultConfiguration(config)
        ViewTarget.setTagId(R.id.glide_tag);
    }

}