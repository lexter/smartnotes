package com.duxcast.smartnotes.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.duxcast.smartnotes.R
import android.net.Uri
import android.graphics.drawable.Animatable
import com.facebook.drawee.controller.BaseControllerListener
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.image.ImageInfo
import kotlinx.android.synthetic.main.activity_imageviewer.*


class ImageViewerActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fresco.initialize(this)
        setContentView(R.layout.activity_imageviewer)

        val controller = Fresco.newDraweeControllerBuilder()
        controller.setUri(Uri.parse(intent.getStringExtra("uri")))
        controller.oldController = photo_drawee_view.controller
        controller.controllerListener = object : BaseControllerListener<ImageInfo>() {
            override fun onFinalImageSet(id: String?, imageInfo: ImageInfo?, animatable: Animatable?) {
                super.onFinalImageSet(id, imageInfo, animatable)
                if (imageInfo == null || photo_drawee_view == null) {
                    return
                }
                photo_drawee_view.update(imageInfo!!.width, imageInfo!!.height)
            }
        }
        photo_drawee_view.controller = controller.build()
    }


}
