package com.duxcast.smartnotes.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.duxcast.smartnotes.R
import kotlinx.android.synthetic.main.activity_view_note.*
import com.duxcast.smartnotes.dagger.ChooseImageDialogModule
import com.duxcast.smartnotes.dagger.ContextModule
import com.duxcast.smartnotes.dagger.DaggerViewNoteActivityComponent
import com.duxcast.smartnotes.models.Note
import io.realm.Realm

class ViewNoteActivity : AppCompatActivity() {

    private var note: Note? = null

    lateinit var realm: Realm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_note)

        DaggerViewNoteActivityComponent.builder()
                .contextModule(ContextModule(this))
                .chooseImageDialogModule(ChooseImageDialogModule())
                .build().inject(this)

        setSupportActionBar(mainToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        realm = Realm.getDefaultInstance()
        note = realm.where(Note::class.java).contains("id", intent.getStringExtra("id")).findFirst()

        if (note == null) {
            finish()
            return
        }

        updateViews()

        if (note!!.photo!=null) {
            mainBackdrop.setOnClickListener({
                val intent = Intent(applicationContext, ImageViewerActivity::class.java)
                intent.putExtra("uri", note!!.photo)
                startActivity(intent)
            })
        }

        fab.setOnClickListener({
            val intent = Intent(applicationContext, EditNoteActivity::class.java)
            intent.putExtra("id", note!!.id)
            startActivity(intent)
        })
    }

    fun updateViews() {
        mainCollapsing.title = note!!.title
        desc_text.text = note!!.text

        when (note!!.priority) {
            1->priority.setBackgroundColor(this@ViewNoteActivity.resources.getColor(R.color.red))
            2->priority.setBackgroundColor(this@ViewNoteActivity.resources.getColor(R.color.yellow))
            3->priority.setBackgroundColor(this@ViewNoteActivity.resources.getColor(R.color.green))
        }

        val options = RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.empty_photo)
                .error(R.drawable.empty_photo_preview)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)

        Glide.with(this@ViewNoteActivity).load(note!!.photo).apply(options).into(mainBackdrop)

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onResume() {
        super.onResume()
        note = realm.where(Note::class.java).contains("id", intent.getStringExtra("id")).findFirst()
        if (note==null) {
            finish()
            return
        }
        updateViews()
    }

}
