package com.duxcast.smartnotes.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import com.duxcast.smartnotes.R
import com.duxcast.smartnotes.adapters.NotesAdapter
import com.duxcast.smartnotes.dagger.ContextModule
import com.duxcast.smartnotes.dagger.DaggerMainActivityComponent
import com.duxcast.smartnotes.models.Note
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    var itemForDelete:ArrayList<Note> = ArrayList()
    lateinit var notesAdapter: NotesAdapter
    lateinit var realm:Realm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        DaggerMainActivityComponent
                .builder()
                .contextModule(ContextModule(this))
                .build()
                .inject(this)

        notesAdapter = NotesAdapter(this)

        recycler.layoutManager = LinearLayoutManager(this)
        recycler.setHasFixedSize(false)
        recycler.adapter = notesAdapter

        val swipeToDismissTouchHelper = ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

                val itemPositionForRemove = viewHolder.adapterPosition
                val itemForRemove = notesAdapter.noteList[itemPositionForRemove]
                notesAdapter.noteList.remove(itemForRemove)
                notesAdapter.notifyItemRemoved(viewHolder.adapterPosition)
                itemForDelete.add(itemForRemove)

                Snackbar.make(recycler, this@MainActivity.getText(R.string.note_deleted), Snackbar.LENGTH_INDEFINITE)
                    .setAction(this@MainActivity.getText(R.string.restore), {
                        notesAdapter.noteList.add(itemPositionForRemove,itemForRemove)
                        notesAdapter.notifyItemInserted(itemPositionForRemove)
                        itemForDelete.remove(itemForRemove)
                    }).show()

            }
        })
        swipeToDismissTouchHelper.attachToRecyclerView(recycler)

        fab.setOnClickListener({
            val intent = Intent(applicationContext,NewNoteActivity::class.java)
            startActivity(intent)
        })

    }

    override fun onPause() {
        super.onPause()
        realm.executeTransaction {
            itemForDelete.forEach({
                it.deleteFromRealm()
            })
        }

    }

    override fun onResume() {
        super.onResume()
        realm = Realm.getDefaultInstance()
        val items: RealmResults<Note> = realm.where(Note::class.java).findAll()
        notesAdapter.clearItems()
        notesAdapter.setItems(items)
        notesAdapter.notifyDataSetChanged()
    }


}
