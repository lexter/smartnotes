package com.duxcast.smartnotes.activities

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.duxcast.smartnotes.R
import kotlinx.android.synthetic.main.activity_new_note.*
import android.content.Intent
import android.support.v7.app.AlertDialog
import android.view.View
import com.bumptech.glide.Glide
import com.duxcast.smartnotes.dagger.ChooseImageDialogModule
import com.duxcast.smartnotes.dagger.ContextModule
import com.duxcast.smartnotes.dagger.DaggerNewNoteActivityComponent
import com.duxcast.smartnotes.models.Note
import io.realm.Realm
import javax.inject.Inject


class NewNoteActivity : AppCompatActivity() {

    @Inject
    lateinit var dialog:AlertDialog;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_note)

        DaggerNewNoteActivityComponent.builder()
                .contextModule(ContextModule(this))
                .chooseImageDialogModule(ChooseImageDialogModule())
                .build().inject(this)

        setSupportActionBar(mainToolbar)
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true)
        getSupportActionBar()!!.setDisplayShowHomeEnabled(true)

        mainBackdrop.setOnClickListener({
            dialog.show()
        })

        var priorityClickListener = View.OnClickListener {
            if (it.isSelected) {
                it.isSelected = false
            } else {
                lowPriority.isSelected = false
                middlePriority.isSelected = false
                highPriority.isSelected = false
                it.isSelected = true
            }
        }

        lowPriority.setOnClickListener(priorityClickListener)
        middlePriority.setOnClickListener(priorityClickListener)
        highPriority.setOnClickListener(priorityClickListener)

        saveButton.setOnClickListener({

            var note: Note = Note()
            note.title = input_title.text.toString()
            note.text = input_desc.text.toString()
            note.photo = mainBackdrop.tag as String?
            note.priority = 0
            when {
                lowPriority.isSelected-> note.priority = 3
                middlePriority.isSelected-> note.priority = 2
                highPriority.isSelected-> note.priority = 1
            }

            var realm:Realm = Realm.getDefaultInstance()
            realm.beginTransaction()
            realm.copyToRealm(note)
            realm.commitTransaction()

            finish()
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 0 || requestCode == 1) {
                Glide.with(this@NewNoteActivity).load(data!!.data).into(mainBackdrop)
                mainBackdrop.tag = data!!.data.toString()
            }
        }
    }
}
