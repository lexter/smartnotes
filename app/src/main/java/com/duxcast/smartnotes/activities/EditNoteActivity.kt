package com.duxcast.smartnotes.activities

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.duxcast.smartnotes.R
import android.content.Intent
import android.support.v7.app.AlertDialog
import android.view.Menu
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.duxcast.smartnotes.dagger.ChooseImageDialogModule
import com.duxcast.smartnotes.dagger.ContextModule
import com.duxcast.smartnotes.dagger.DaggerEditNoteActivityComponent
import com.duxcast.smartnotes.models.Note
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_edit_note.*
import javax.inject.Inject




class EditNoteActivity : AppCompatActivity() {

    @Inject
    lateinit var dialog:AlertDialog;
    lateinit var realm:Realm
    var note: Note? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_note)

        DaggerEditNoteActivityComponent.builder()
                .contextModule(ContextModule(this))
                .chooseImageDialogModule(ChooseImageDialogModule())
                .build().inject(this)

        setSupportActionBar(mainToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        mainToolbar.setOnMenuItemClickListener({
            if (it.itemId == R.id.menu_delete) {
                val builder = AlertDialog.Builder(this)

                builder.setTitle(R.string.confirm)
                builder.setMessage(R.string.are_you_sure)

                builder.setPositiveButton(getText(R.string.yes)) { dialog, which ->
                    dialog.dismiss()
                    realm.executeTransaction {
                        note!!.deleteFromRealm()
                        this@EditNoteActivity.finish()
                    }
                }

                builder.setNegativeButton(getText(R.string.no)) { dialog, which ->
                    dialog.dismiss()
                }

                val alert = builder.create()
                alert.show()
            }
            true
        })


        realm = Realm.getDefaultInstance()
        note = realm.where(Note::class.java).contains("id", intent.getStringExtra("id")).findFirst()

        if (note == null) {
            finish()
            return
        }

        input_title.setText(note!!.title)
        input_desc.setText(note!!.text)

        when(note!!.priority) {
            3->lowPriority.isSelected = true
            2->middlePriority.isSelected = true
            1->highPriority.isSelected = true
        }

        val options = RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.empty_photo_preview)
                .error(R.drawable.empty_photo)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)

        Glide.with(this@EditNoteActivity).load(note!!.photo).apply(options).into(mainBackdrop)

        mainBackdrop.tag = note!!.photo

        mainBackdrop.setOnClickListener({
            dialog.show()
        })

        var priorityClickListener = View.OnClickListener {
            if (it.isSelected) {
                it.isSelected = false
            } else {
                lowPriority.isSelected = false
                middlePriority.isSelected = false
                highPriority.isSelected = false
                it.isSelected = true
            }
        }

        lowPriority.setOnClickListener(priorityClickListener)
        middlePriority.setOnClickListener(priorityClickListener)
        highPriority.setOnClickListener(priorityClickListener)

        saveButton.setOnClickListener({

            var realm:Realm = Realm.getDefaultInstance()
            realm.executeTransaction {
                note!!.title = input_title.text.toString()
                note!!.text = input_desc.text.toString()
                note!!.photo = mainBackdrop.tag as String?
                note!!.priority = 0
                when {
                    lowPriority.isSelected-> note!!.priority = 3
                    middlePriority.isSelected-> note!!.priority = 2
                    highPriority.isSelected-> note!!.priority = 1
                }

                it.insertOrUpdate(note)
            }

            finish()
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.edit_note_menu, menu)
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 0 || requestCode == 1) {
                Glide.with(this@EditNoteActivity).load(data!!.data).into(mainBackdrop)
                mainBackdrop.tag = data!!.data.toString()
            }
        }
    }
}
