package com.duxcast.smartnotes.adapters

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.duxcast.smartnotes.R
import com.duxcast.smartnotes.models.Note
import android.view.LayoutInflater
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AlertDialog
import android.support.v7.widget.CardView
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.duxcast.smartnotes.activities.ViewNoteActivity
import com.duxcast.smartnotes.activities.MainActivity
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.duxcast.smartnotes.activities.EditNoteActivity

class NotesAdapter(var context:Context): RecyclerView.Adapter<NotesAdapter.NoteViewHolder>() {


    val noteList = ArrayList<Note>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {

        val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.note_item, parent, false)
        return NoteViewHolder(context,view)

    }

    override fun onBindViewHolder(noteViewHolder: NoteViewHolder, position: Int) {
        noteViewHolder.bind(noteList[position])
    }


    override fun getItemCount(): Int {
        return noteList.size
    }

    fun setItems(notes: Collection<Note>) {
        noteList.addAll(notes)
        notifyDataSetChanged()
    }

    fun clearItems() {
        noteList.clear()
        notifyDataSetChanged()
    }

    class NoteViewHolder(var context:Context, view:View): RecyclerView.ViewHolder(view) {

        var cardView:CardView = view.findViewById(R.id.cardView)
        var image:ImageView = view.findViewById(R.id.image)
        var nameTextView:TextView = view.findViewById(R.id.nameTextView)
        var priority:View = view.findViewById(R.id.priority)

        fun bind(note:Note) {
            cardView.setOnClickListener({
                var intent = Intent(context, ViewNoteActivity::class.java)
                intent.putExtra("id",note.id)
                (context as MainActivity).startActivity(intent)
            })

            cardView.setOnLongClickListener({
                val builder: AlertDialog.Builder = AlertDialog.Builder(context)
                builder.setTitle(R.string.menu).setItems(R.array.long_click_dialog, { _: DialogInterface, i: Int ->
                    when {
                        (i==0) -> {
                            var intent = Intent(context, EditNoteActivity::class.java)
                            intent.putExtra("id",note.id)
                            (context as Activity).startActivity(intent)
                        }
                        (i==1) -> {
                            val sharingIntent = Intent(android.content.Intent.ACTION_SEND)
                            sharingIntent.type = "text/plain"
                            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, note.title)
                            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, note.text)
                            (context as Activity).startActivity(Intent.createChooser(sharingIntent, context.getString(R.string.sharevia)))
                        }
                    }
                }).show()
                true
            })

            nameTextView.text = note.title

            when (note.priority) {
                1->priority.background = context.getDrawable(R.drawable.high_priority_bg)
                2->priority.background = context.getDrawable(R.drawable.middle_priority_bg)
                3->priority.background = context.getDrawable(R.drawable.low_priority_bg)
            }

            val options = RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.empty_photo_preview)
                    .error(R.drawable.empty_photo_preview)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)

            Glide.with(context).load(note.photo).apply(options).into(image)
        }
    }

}